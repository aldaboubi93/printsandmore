<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Category;
use App\Http\Resources\CategoryResource;
use App\Http\Resources\ItemsResource;
use App\Http\Resources\ObjectsResource;
use App\Http\Resources\SubCategoryResource;
use App\Http\Resources\UserResource;
use App\Item;
use App\PObject;
use App\MembersAddress;
use App\Order;
use App\Subcategory;
use App\Member;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

Route::get('/', function () {

    $users = DB::select(\Illuminate\Support\Facades\DB::RAW("select * from users"));
    return $users;
});

Route::get('api/categories', function (){
//    $allCategories =
    $categories =  Category::all();
    return $categories;
});

Route::get('api/subcategory', function (Request $request){
    $cat_id = $request->cat_id;
    $allSubCategories = SubCategory::where('cat_id',$cat_id)->get();
    $subcategories = $allSubCategories;
    return $subcategories;
});


Route::get('api/items', function (Request $request){
    $sub_id = $request->sub_id;
    $allItems = Item::where('sub_id',$sub_id)->get();
    return $allItems;
});

Route::get('api/objects', function (Request $request){
    $item_id = $request->item_id;
    $allObjects = PObject::where('item_id',$item_id)->get();
    return $allObjects;
});


Route::post('api/user/register', function(Request $request){
    $name = $request->name;
    $last_name = $request->last_name;
    $email = $request->email;
    $password = $request->password;
    $mobile = $request->mobile;
    $university_name = $request->university_name;
    $academic_year = $request->academic_year;
    $specialization = $request->specialization;
    $user_lat = $request->user_lat;
    $user_longi = $request->user_longi;
    $address_name = $request->address_name;
    $city = $request->city;
    $address_details = $request->address_details;

    $token = sha1(time());

    $checkUser = Member::where("email",$email)->get();


    if (count($checkUser) == 0){
        $newUser = new Member();
        $newUser->name = $name;
        $newUser->last_name = $last_name;
        $newUser->email = $email;
        $newUser->password = $password;
        $newUser->mobile = $mobile;
        $newUser->university_name = $university_name;
        $newUser->academic_year = $academic_year;
        $newUser->specialization = $specialization;
        $newUser->user_lat = $user_lat;
        $newUser->user_longi = $user_longi;
        $newUser->token = $token;


        if($newUser->save()){
            $member_address = new MembersAddress();
            $member_address->address_name = $address_name;
            $member_address->city = $city;
            $member_address->address_details = $address_details;
            $member_address->member_id = $newUser->id;
            $member_address->save();
        }

        $getUser = Member::where("id",$newUser->id)->with('addresses')->get();
        return $getUser;
    } else {
        return response()->json(array(
            'code'      =>  409,
            'message'   =>  'this email already exist'
        ), 409);
    }
});


Route::post('api/user/login', function(Request $request){

    $email = $request->email;
    $password = $request->password;

    $checkUser = Member::where("email",$email)->where('password',$password)->get();


    if ($checkUser){
        return $checkUser;
//        return count($checkUser);
    } else {
        return "error";
    }


});

Route::get('api/user/addresses', function(Request $request){

    $uid = $request->uid;

    $userAddresses = MembersAddress::where("member_id",$uid)->get();
    
    if ($userAddresses){
        return response()->json($userAddresses);
    } else {
        return response()->json(['message' => 'have error'], 422);
    }
});

Route::post('api/user/add_address', function (Request $request){
    $address_name = $request->address_name;
    $city = $request->city;
    $address_details = $request->address_details;
    $uid = $request->uid;

    $newAddress = new MembersAddress();
    $newAddress->address_name = $address_name;
    $newAddress->city = $city;
    $newAddress->address_details = $address_details;
    $newAddress->member_id = $uid;

    if ($newAddress->save()){
        return response()->json($newAddress);
    } else {
        return response()->json(['message' => 'have error'], 422);
    }

});

Route::post('api/user/address/delete', function (Request $request){
    $address_id = $request->address_id;

    $address = MembersAddress::find($address_id);

    if($address->delete()){
        return response()->json(['message' => 'deleted address'], 200);
    } else {
        return response()->json(['message' => 'have error'], 422);
    }
});


Route::get('api/slider', function(){

    $slider = \App\Slider::all();

    return $slider;
});


Route::get('api/videos', function(Request $request){

//    $obj_id = $request->obj_id;

//    $videos = \App\Video::where("item_id",$obj_id)->get();

//    return $videos;
});



Route::get('api/videos', function(Request $request){

    $obj_id = $request->obj_id;

    $videos = \App\Video::where("item_id",$obj_id)->get();

    return $videos;
});

Route::post('api/order', function (Request $request){

    if ($request->user_address == 'Select Your address'){
        return response()->json(['message' => 'Please Select Address'], 401);
    } else {
        $status = 0;

        $newOrder = new \App\Order();
        $newOrder->user_id = $request->user_id;
        $newOrder->user_address = $request->user_address;
        $newOrder->user_lat = $request->user_lat;
        $newOrder->user_longi = $request->user_longi;
        if ($newOrder->save()) {
            $status = 1;
        }


        $items = $request->products;
        foreach ($items as $item) {
            $value = explode(',', $item);
            $newOrderItems = new \App\ObjItem();
            $newOrderItems->obj_name = $value[0];
            $newOrderItems->qty = $value[1];
            $newOrderItems->order_id = $newOrder->id;
            $newOrderItems->save();
        }


        if ($status == 1) {
            return response()->json(['msg' => 'success order', 'code' => '200']);
        } else {
            return response()->json(['msg' => 'error order', 'code' => '409']);
        }
    }


});




Route::post('api/user/khalid', 'ApiController@make_order');



Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
