<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Member extends Model
{
    public function addresses()
    {
        return $this->hasMany('App\MembersAddress', 'member_id');
    }
}
